// Store for all kinds of data :-)
let menu = {
    // HTML container for the whole template
    container: null,
    // HTML container for the menu
    menuContainer: null,
    // HTML element representing the menu
    menuElement: null,
    // Menu item definitions
    items: [],
    // HTML elements which will change color when menu items are hovered over
    flashyThings: []
};


// Menu initialization
function initializeMenu() {
    // Get HTML elements representing menu container and text container
    menu.container = document.querySelector('#laytheme');
    menu.menuContainer = menu.container.querySelector('.menu-container');
    menu.menuElement = menu.container.querySelector('.menu');

    // Get HTML elements which will change color when menu items are hovered over
    menu.flashyThings = menu.container.querySelectorAll('.dynamic-color');

    // Get HTML elements representing menu items
    let items = menu.container.querySelectorAll('.menu-item');
    for (let i = 0; i < items.length; i++) {
        // Collect the parameters of a menu item
        let item = { element: items[i] };
        menu.items.push(item);
        item.labels = item.element.querySelectorAll('p');
        item.backgroundImage = { url: item.element.getAttribute('background-image') };
        item.menuColor = item.element.getAttribute("menu-color");

        // Prefetch background image to avoid flickering
        if (item.backgroundImage.url) {
            item.backgroundImage.data = new Image();
            item.backgroundImage.data.src = item.backgroundImage.url;
        }
    }


    // With a bit of delay, attach mouse event handlers to menu item.
    // When menu item is selected or hovered over:
    // 1. Change container background to the one associated with the item
    // 2. Change the text color of all menu labels
    window.setTimeout(() => {
        // Wire up menu items
        for (let item of menu.items) {
            item.element.addEventListener('mouseover', () => selectMenuItem(item));
            item.element.addEventListener('click', () => selectMenuItem(item));
        }

        // Select the first menu item
        selectMenuItem(menu.items[0]);
    }, 1000);
}


// Called when menu item has been selected
function selectMenuItem(data) {
    if (data) {
        // Change page background image, if defined
        if (data.backgroundImage.url) {
            menu.container.style.backgroundImage = "url('" + data.backgroundImage.url + "')";
        }
        // Change text color of all items tagged with dynamic-color class, if color defined
        if (data.menuColor) {
            for (let thing of menu.flashyThings) {
                thing.style.color = data.menuColor;
            }
        }
    }
}


// Initialize the menu only after the page has finished loading!
window.addEventListener('load', function() {
    initializeMenu();
});
